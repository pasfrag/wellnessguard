from logging.config import fileConfig
from sqlalchemy import engine_from_config
from sqlalchemy import pool

from alembic import context

# Import your SQLAlchemy models from the 'crud' directory
from app.dependencies.database import Base
from app.crud.user import User
from app.crud.oauth import OAuth
# Load the Alembic config object from alembic.ini
from alembic.config import Config


alembic_cfg = Config(context.config.config_file_name)

# Retrieve the database URL from alembic.ini
db_url = alembic_cfg.get_main_option("sqlalchemy.url")

# Set the database URL in Alembic's context
context.config.set_main_option("sqlalchemy.url", db_url)

# This is the Alembic Config object, which provides access to the Alembic configuration.
config = context.config

# Interpret the config file for Python logging.
fileConfig(config.config_file_name)

# Add your models for autogeneration support.
# By setting the target_metadata, Alembic can automatically generate migrations.
target_metadata = [Base.metadata]

# This section ensures that Alembic works correctly with different database engines.
# You may need to adjust the imports based on your specific database configuration.
def run_migrations_offline():
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()

def run_migrations_online():
    # Connect to the database using the configured URL.
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            transactional_ddl=True,
        )

        with context.begin_transaction():
            context.run_migrations()

# If the script is run as a main module (e.g., `alembic upgrade head`),
# Alembic will call the appropriate function based on whether the database
# is available or not.
if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
