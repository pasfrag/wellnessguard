from dotenv import find_dotenv, load_dotenv
import os


class AppConfig:
    load_dotenv(find_dotenv())
    DEBUG = True
    DATABASE_URL = os.environ.get("DATABASE_URL")
    USER_SECRET = os.environ.get("USER_SECRET")
    TOKEN_ALGORITHM = os.environ.get("TOKEN_ALGORITHM")


config = AppConfig()
