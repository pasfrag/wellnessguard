from pydantic import BaseModel


class OAuth(BaseModel):
    user_id: int
    password: str
