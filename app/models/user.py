from pydantic import BaseModel


class User(BaseModel):
    username: str
    email: str


class FullUser(User):
    id: int

    class Config:
        orm_mode = True
