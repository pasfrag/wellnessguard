from app.models.user import User


class CreateUserRequest(User):
    password: str

    def get_user(self) -> User:
        return User(**self.dict())
