from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from app.config import config

# Define the database URL based on the configuration
DATABASE_URL = config.DATABASE_URL

# Create a SQLAlchemy engine
engine = create_engine(DATABASE_URL)

# Create a Session class for the database
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Base class for SQLAlchemy models
Base = declarative_base()

metadata = Base.metadata

db = SessionLocal()
