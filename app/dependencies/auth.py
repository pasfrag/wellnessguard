from fastapi import Depends, HTTPException, status
from app.models.user import User
from app.config import config


USER_SECRET = config.USER_SECRET
TOKEN_ALGORITHM = config.TOKEN_ALGORITHM
