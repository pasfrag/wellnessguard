from sqlalchemy import Column, Integer, String
from app.dependencies.database import Base, db, metadata
from app.models.user import FullUser, User as UserModel
from typing import Optional


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True, nullable=False, autoincrement=True)
    username = Column(String, unique=True, index=True, nullable=False)
    email = Column(String, unique=True, index=True, nullable=False)


def get_user_by_id(user_id: int) -> Optional[FullUser]:
    user = db.query(User).get(user_id)
    return convert_to_user(user)


def get_user_by_username(username: str) -> Optional[FullUser]:
    user = db.query(User).filter(User.username == username).first()
    return convert_to_user(user)


def get_user_by_email(email: str) -> Optional[FullUser]:
    user = db.query(User).filter(User.email == email).first()
    return convert_to_user(user)


def create_user(user_dto: UserModel) -> FullUser:
    user_dict = user_dto.dict()

    user = User(**user_dict)
    db.add(user)
    db.commit()
    db.refresh(user)

    return FullUser.from_orm(user)


def convert_to_user(user: User) -> Optional[FullUser]:
    if user is None:
        return None
    return FullUser.from_orm(user)
