from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from app.dependencies.database import Base, db, metadata
from app.models.oauth import OAuth as OAuthModel


class OAuth(Base):
    __tablename__ = 'oauth'

    user_id = Column(Integer, ForeignKey("users.id"), index=True, primary_key=True)
    password = Column(String, nullable=False)

    relationship("User", back_populates="oauth")

    class Config:
        orm_mode = True


def get_password_by_user_id(user_id: int) -> str:
    oauth = db.query(OAuth).get(user_id)
    if oauth is None:
        return None
    return oauth.password


def create_oauth(oauth_dto: OAuthModel) -> None:
    oauth_dict = oauth_dto.dict()
    oauth = OAuth(**oauth_dict)

    db.add(oauth)
    db.commit()
