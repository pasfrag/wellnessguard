from fastapi import APIRouter
from app.models.user import FullUser
from app.models.request_models.create_user_request import CreateUserRequest
from app.services.user_service import create_new_user

router = APIRouter()


@router.post("/user", response_model=FullUser)
async def post_user(user: CreateUserRequest) -> FullUser:
    return create_new_user(user)

