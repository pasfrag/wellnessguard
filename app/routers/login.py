from fastapi import APIRouter
from app.models.request_models.login_user_request import LoginUserRequest
from app.models.token import Token
from app.models.user import FullUser
from app.services.authentication_service import authenticate_user, create_jwt_token
from app.services.user_service import get_user_with_user_identifier

router = APIRouter()


@router.post("/login", response_model=Token)
async def login_user(login_data: LoginUserRequest) -> Token:
    full_user = get_user_with_user_identifier(login_data.username)
    authenticate_user(full_user.id, login_data.password)
    return create_jwt_token(data={"sub": full_user.username})
