from fastapi import HTTPException


class UserAlreadyExistsException(HTTPException):
    def __init__(self, message: str):
        super().__init__(status_code=409, detail=message)


class UserDoesNotExistException(HTTPException):
    def __init__(self, message: str):
        super().__init__(status_code=404, detail=message)


class IncorrectUserPasswordCombinationException(HTTPException):
    def __init__(self):
        super().__init__(status_code=403, detail="Incorrect username/password combination.")
