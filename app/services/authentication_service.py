from datetime import timedelta
from typing import Optional

import bcrypt
from app.exceptions.custom_exceptions import IncorrectUserPasswordCombinationException
from app.models.oauth import OAuth
from app.crud.oauth import create_oauth, get_password_by_user_id
from app.dependencies.auth import USER_SECRET, TOKEN_ALGORITHM
from app.models.token import Token
from jose import JWTError, jwt


def create_password(oauth: OAuth) -> None:
    hashed_password = hashing_password(oauth.password)
    oauth.password = hashed_password
    create_oauth(oauth)


def hashing_password(password: str) -> str:
    user_password = password.encode('utf-8')  # Convert to bytes
    salt = bcrypt.gensalt()
    hashed_password = bcrypt.hashpw(user_password, salt)
    return hashed_password.hex()


def authenticate_user(user_id: int, entered_password: str) -> bool:
    password = get_password_by_user_id(user_id)
    hashed_password = bytes.fromhex(password)

    entered_password = entered_password.encode("utf-8")
    if not bcrypt.checkpw(entered_password, hashed_password):
        raise IncorrectUserPasswordCombinationException()


def create_jwt_token(data: dict) -> Token:
    token = jwt.encode(data, USER_SECRET, algorithm=TOKEN_ALGORITHM)
    return Token(access_token=token, token_type="bearer")
