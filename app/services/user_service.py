from app.models.request_models.create_user_request import CreateUserRequest
from app.models.oauth import OAuth
from app.crud.user import create_user, get_user_by_username, get_user_by_email
from app.models.user import FullUser
from app.services.authentication_service import create_password
from app.exceptions.custom_exceptions import UserAlreadyExistsException, UserDoesNotExistException


def create_new_user(create_user_request: CreateUserRequest) -> FullUser:
    validate_user(create_user_request)
    user = create_user_request.get_user()
    user = create_user(user)

    oauth = OAuth(user_id=user.id, password=create_user_request.password)
    create_password(oauth)

    return user


def validate_user(create_user_request: CreateUserRequest) -> None:
    validate_username(create_user_request.username)
    validate_email(create_user_request.email)


def validate_username(username: str) -> None:
    user = get_user_by_username(username)
    if user is not None:
        raise UserAlreadyExistsException("Username already exists.")


def validate_email(email: str) -> None:
    user = get_user_by_email(email)
    if user is not None:
        raise UserAlreadyExistsException("Email already exists.")


def get_user_with_user_identifier(user_identifier: str) -> FullUser:
    user = get_user_by_email(user_identifier)
    if user is not None:
        return user

    user = get_user_by_username(user_identifier)
    if user is not None:
        return user

    raise UserDoesNotExistException("User with this email/username does not exist.")
