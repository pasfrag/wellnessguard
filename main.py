from fastapi import FastAPI
from app.routers import user, login

app = FastAPI()

# Include API routers
app.include_router(user.router)
app.include_router(login.router)
